<?php

namespace app\index\controller;

use QL\QueryList;

class Index
{
    public function index()
    {
        echo 'index-index';

    }

    public function hello($name = 'Swoole')
    {
        echo 'hello,' . $name;
    }

    public function wecot()
    {
        echo 'wecot-wecot';
    }

    public function query()
    {
        $html = <<<STR
<div id="one">
    <div class="two">
        <a href="http://querylist.cc">QueryList官网</a>
        <img src="http://querylist.com/1.jpg" alt="这是图片">
        <img src="http://querylist.com/2.jpg" alt="这是图片2">
    </div>
    <span>其它的<b>一些</b>文本</span>
</div>        
STR;
        $rules = array(
            //采集id为one这个元素里面的纯文本内容
            'text' => array('#one','text'),
            //采集class为two下面的超链接的链接
            'link' => array('.two>a','href'),
            //采集class为two下面的第二张图片的链接
            'img' => array('.two>img:eq(1)','src'),
            //采集span标签中的HTML内容
            'other' => array('span','html')
        );
        $data = QueryList::Query($html,$rules)->data;

        pp($data);
    }
    public function ql()
    {
        $targetUrl = 'http://www.nipic.com/photo/renwu/index.html';

        $rule = [
            'beauty-images'=>['img', 'src']
        ];
       $data = QueryList::Query($targetUrl, $rule);
       pp($data);
    }

    /**
     * @throws \Exception
     */
    public function dlimg()
    {
        $targetUrl = 'http://www.27270.com/tag/777.html';
        $html = QueryList::run('DImage',[
            //html内容
            'content' => file_get_contents($targetUrl),
            //图片保存路径（相对于网站跟目录），可选，默认:/images
            'image_path' => '/runtime/images/',
            //网站根目录全路径，如:/var/www/html
            'www_root' => dirname(dirname(dirname(dirname(__FILE__)))),
            //补全HTML中的图片路径,可选，默认为空
            'base_url' => '',
            //图片链接所在的img属性，可选，默认src
            //多个值的时候用数组表示，越靠前的属性优先级越高
            'attr' => array('data-src','src'),
            //单个值时可直接用字符串
            //'attr' => 'data-src',
            //回调函数，用于对图片的额外处理，可选，参数为img的phpQuery对象
            'callback' => function($imgObj){

            }
        ]);
        print_r($html);
    }

    /**
     * @throws \Exception
     */
    public function multiThread()
    {
        QueryList::run('Multi',[
            //待采集链接集合
            'list' => [
                'http://cms.querylist.cc/news/it/547.html',
                'http://cms.querylist.cc/news/it/545.html',
                'http://cms.querylist.cc/news/it/543.html'
                //更多的采集链接....
            ],
            'curl' => [
                'opt' => array(
                    //这里根据自身需求设置curl参数
                    CURLOPT_SSL_VERIFYPEER => false,
                    CURLOPT_SSL_VERIFYHOST => false,
                    CURLOPT_FOLLOWLOCATION => true,
                    CURLOPT_AUTOREFERER => true,
                    //........
                ),
                //设置线程数
                'maxThread' => 100,
                //设置最大尝试数
                'maxTry' => 3
            ],
            'success' => function($a){
                //采集规则
                $reg = array(
                    //采集文章标题
                    'title' => array('h1','text'),
                    //采集文章正文内容,利用过滤功能去掉文章中的超链接，但保留超链接的文字，并去掉版权、JS代码等无用信息
                    'content' => array('.post_content','html','a -.content_copyright -script' )
                );
                $rang = '.content';
                $ql = QueryList::Query($a['content'],$reg,$rang);
                $data = $ql->getData();
                //打印结果，实际操作中这里应该做入数据库操作
                print_r($data);
            }
        ]);
    }
}
